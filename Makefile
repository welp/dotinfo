#!/usr/bin/make -f

install:
	cd static; compass compile -e production --force
	hugo --source=/Users/alexd/Dropbox/sites/alexdunn \
	--destination=/Users/alexd/Sites/alexdunn.info

clean:
	rm -drf public
	find . -name "*~" -exec rm {} \;
