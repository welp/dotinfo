---
title: "Curriculum Vitae"
date: 2014-08-05
tag: bio
    - evergreen
---

### Contact

- Department of Philosophy<br />
  South Hall 5706<br />
  UCSB<br />
  Santa Barbara, CA 93106-3090

- <adunn@umail.ucsb.edu>

- <http://alexdunn.info>

### Education

- PhD in Philosophy from the
[University of California, Santa Barbara](http://philosophy.ucsb.edu)
(Expected in 2017)

- BA in Philosophy from
  [Reed College](http://academic.reed.edu/philosophy/) (2012)

<!--more-->

### Teaching

- Teaching Assistant for [Philosophy 20A](http://philosophy.ucsb.edu/courses/f14/phil20a/) with [Voula Tsouna](http://philosophy.ucsb.edu/fac/voula-tsouna/) in Fall 2014

- Teaching Assistant for [Philosophy 1](http://philosophy.ucsb.edu/courses/s14/phil1/) with [Tim Butzer](http://philosophy.ucsb.edu/grad/tim-butzer/) in Spring 2014

- Teaching Assistant for [Philosophy 1](http://www.philosophy.ucsb.edu/courses/winter_2014/index.html#philosophy-1-introduction-to-philosophy/) with [Jenessa Strickland](http://philosophy.ucsb.edu/grad/jenessa-strickland/) in Winter 2014

- Teaching Assistant for [Philosophy 4](http://www.philosophy.ucsb.edu/courses/fall_2013.html) with [Tim Butzer](http://philosophy.ucsb.edu/grad/tim-butzer/) in Fall 2013

### Publications

- ["The Metaphysics of Ordinary Things"](thesis.html), senior thesis, Reed College (2012)

    *Abstract:* Theories of metaphysical nihilism claim that there are
    no (or nearly no) objects with parts: no chairs, houses,
    mountains, and perhaps even no people.  The philosophers who make
    these claims have trouble explaining why we nonetheless believe
    there are such things.  The only successful explanation is
    compatible both with nihilism and with metaphysical universalism.
    Universalism claims that for every set of things, there is
    something else made up of those things; this thesis is intuitively
    more plausible than nihilism.  But if we assume that universalism
    is true, and if we do not presuppose four-dimensionalism, we have
    to choose between two unintuitive versions of universalism: one
    that posits a plurality of co-located (entirely overlapping)
    objects, or one that denies that things can change their parts.

### Service to the profession

- [UCSB Philosophy Department Website](http://www.philosophy.ucsb.edu)

    During the Winter 2014 quarter, I rewrote the UCSB Philosophy website from
    the ground up, making it vastly more responsive and accessible. As of July
    2014, I am the webmaster for the Philosophy department.

- [*Modern Philosophy* online text](baruffio.com/modernphilosophy/text.html 'HTML version of Modern Philosophy')

    *Modern Philosophy* is a free textbook created by
    [Walter Ott](https://filebox.vt.edu/users/ottw/ott.htm "Walter
    Ott's personal site"). It combines public-domain primary sources
    with supplementary material and study questions.

    In the summer of 2012 I produced a version of this textbook in
    [Markdown](http://daringfireball.net/projects/markdown/ "Markdown
    project page"). Markdown is a plain-text syntax that allows for
    easy conversion to other file formats.  Using
    [pandoc](http://johnmacfarlane.net/pandoc/ "pandoc project page"),
    *Modern Philosophy* can be easily converted to HTML, PDF, EPUB,
    and
    [many other formats](http://johnmacfarlane.net/pandoc/README.html#options
    "pandoc options").

### Honors & awards

#### 2013-2014

- Burnand-Partridge Foundation Scholarship (SB Scholarship Foundation)

#### 2012--2013

- R.W. Church Scholarship (UCSB, Spring 2013)

- Burnand-Partridge Foundation Scholarship (SB Scholarship Foundation)

#### 2011--2012

- Robert O. Dougan Memorial Scholarship (SB Scholarship Foundation)

#### 2010--2012

- Commended for Excellence in Scholarship (Reed College)

- Mary E. Barnard Memorial Humanities Scholarship (Reed College)

#### 2010--2011

- David D. Stalker Memorial Scholarship (SB Scholarship Foundation)
