---
title: "The Trademark Argument for the Existence of God"
author: "Alex Dunn"
date: 2014-02-06
course: phil1
quarter: w14
---

Descartes claims that "there must be at least as much in the cause as
in the effect of that cause."  His examples in the third *Meditation*
include the following:

- "A stone cannot begin to exist unless it is produced by something
  which contains everything to be found in the stone."

- "Heat cannot be produced in an object except by something of at
  least the same degree or kind of perfection as heat."

<!--more-->

### The No-Something-From-Nothing Principle

The examples above make some sense: to produce sedimentary rock, you need
at least as much sediment as ends up in the rock; to produce heat, you
need at least as much energy as corresponds to the heat produced.
Otherwise you would be creating something from nothing, which
Descartes thinks is impossible.

Now Descartes claims that this principle applies to ideas as well:

> The ideas in me are like images which can easily fall short of the
> perfection of the things from which they are taken, but which cannot
> contain anything greater or more perfect.

My ideas of imperfect things might be cobbled together from imperfect
sources (my imagination, or even myself), but an idea of absolute
perfection cannot.  It needs a perfect source.

Do I have such an idea?

### The Perfect Idea

> How could I understand that I ... lacked something ... that I was
> not wholly perfect, unless there were in me some idea of a more
> perfect being which enabled me to recognize my own defects by
> comparison?

I think what Descartes is getting at is something like this:

- I can recognize imperfections in myself.

- To do that, I must have an idea of something *relatively more*
  perfect than I am.  Call that relatively more perfect thing "Dog".

Now we are faced with a question: is Dog absolutely perfect, or only
relatively more perfect than me?

- Is Dog absolutely perfect?  Then I have an idea of something
  absolutely perfect.

- Is Dog *not* absolutely perfect?  If I can determine that Dog is not
  absolutely perfect, then I must be able to recognize imperfections
  in Dog.  *To do that*, I must have an idea of something relatively
  more perfect than Dog.  Call that relatively more perfect thing
  "Dogg".

Now we are faced with the same question again.  Is Dogg absolutely
perfect, or only relatively more perfect than Dog?

Descartes claims that if we keep asking ourselves this question,
eventually we will be forced to admit that we really do have an idea
of something absolutely perfect.

****

Now put this together with our No-Something-From-Nothing principle
from above.  An idea of absolute perfection must have come from
something absolutely perfect.  We have an idea of absolute perfection,
which means there is something absolutely perfect.  Descartes believes
that God is whatever is absolutely perfect, so he considers this to be
a proof of the existence of God.
