---
title: "One person&#8217;s <em>modus ponens</em> is another&#8217;s <em>modus tollens</em>"
author: "Alex Dunn"
date: 2014-04-23
course: phil1
quarter: s14
---

William Rowe pointed out that you can take his argument against the
existence of God and basically reverse it:

1. Unjustified evil exists (*premise*).

2. If God existed, then no unjustified evil would exist (*premise*).

3. Therefore, God does not exist (*follows from 1 and 2 by* modus tollens).

To reverse this argument, we have to begin with different premises.
Instead of assuming that unjustified evil exists, we assume that God
exists:

1. God exists (*premise*).

2. If God exists, then no unjustified evil would exist (*premise*).

3. Therefore, no unjustified evils exist (*follows from 1 and 2 by*
   modus ponens).

<!--more-->

We kept the conditional *if God exists, then no unjustified evil would
exist,* but used it to make a different inference.  Rowe's argument
used premise 2 to make a *modus tollens* inference, from the existence
of unjustified evil to the non-existence of God.  The theist's
argument uses the very same premise 2 to make a *modus ponens*
inference, from the existence of God to the non-existence of
unjustified evil.

In a situation like this, both sides are using valid arguments to draw
contrary conclusions.  The only thing to do is decide who has the more
plausible *premises*.  Is is more likely that unjustified evil exists,
or that God exists?  There's going to be a lot of disagreement; it's
not obvious which direction---*modus ponens* or *modus tollens*---is
more justified.

******

But there are other cases like this in which it's more clear which is
the right direction.  Remember Norcross' argument against supporting
factory farms:

1. If it is wrong to torture puppies for gustatory pleasure, then it
   is wrong to support factory farming (*premise*).

2. It is wrong to torture puppies for gustatory pleasure (*premise*).

3. Therefore, it is wrong to support factory farming (*follows from 1
   and 2 by* modus ponens).

Norcross showed how someone might reverse this argument:

1. If it is wrong to torture puppies for gustatory pleasure, then it
   is wrong to support factory farming (*premise*).

2. It is not wrong to support factory farming (*premise*).

3. Therefore, it is not wrong to torture puppies (*follows from 1 and
   2 by* modus tollens).

Here we have the same sort of stalemate: both sides have a valid
argument for their preferred conclusion.  So we have to step back
again and decide who has the more plausible set of premises.  They
both agree on the conditional premise that *if it is wrong to torture
puppies for gustatory pleasure, then it is wrong to support factory
farming*.  But Norcross takes it as a premise that it is wrong to
torture puppies, while his hypothetical opponent takes it as a premise
that it is *not* wrong to support factory farming.  Which of *these*
premises is more likely to be true?

*******

Peter Singer presents this argument (the way I've written it down
should make it more clear that it's valid):

1. If it is in our power to prevent something bad from happening,
   without thereby sacrificing anything of comparable moral
   importance, we ought, morally, to do it (*premise*).

2. Suffering and death from lack of food, shelter, and medical care
   are bad (*premise*).

3. If it is in our power to prevent suffering and death from lack of
   food, shelter, and medical care from happening, without thereby
   sacrificing anything of comparable moral importance, we ought,
   morally, to do it (*follows from premises 1 and 2 by
   substitution*).

4. It is in our power to prevent suffering and death from lack of
   food, shelter, and medical care from happening, without thereby
   sacrificing anything of comparable moral importance (*premise*).

5. We ought, morally, to prevent suffering and death from lack of
   food, shelter, and medical care from happening (*follows from
   3 and 4 by* modus ponens).

Singer's conclusion is very demanding; if it's true, then we are all
failing to do our duty.  We are falling short as moral agents.

But maybe Singer's conclusion isn't true.  Maybe we can perform a
similar reversal on his argument.  Suppose we take it as a premise
that we're not always required to prevent suffering and death around
the world.  Maybe we are sometimes, in some places, but let's take as
a premise that it's not *universally* required, which is what Peter
Singer thinks.

Now since we're denying the conclusion of a valid argument, we have to
deny one of the argument's premises.  Which one might be false?
