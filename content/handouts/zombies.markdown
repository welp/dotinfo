---
title: "Zombies!"
author: "Alex Dunn"
date: 2014-02-13
course: phil1
quarter: w14
---

A zombie, in the philosophical sense, is something that's physically
indistinguishable from a human but has no *mental states*.

Other, maybe less precise ways of getting at this, would be to say
that zombies are not *conscious*, or that there's *nothing going on up
there.* (Of course there are lots of physical things going on up
there, but no consciousness.)

<!--more-->

But even though zombies don't have any mental states, they *act as if
they do*.  In the same sorts of situations where we would laugh, cry,
and so on, they would laugh, cry, and so on.  But here's the
difference:

- When *we* laugh, it's because we're happy or amused.  When *zombies*
  laugh, it's because they're in a situation that would make a human
  happy, and because they're built to laugh when they're in a
  situation that would make a human happy.

- When *we* cry, it's because we're sad.  When *zombies* cry, it's
  because they're in a situation that would make a human sad, and
  because they're built to cry when they're in a situation that
  would make a human sad.

Zombies can also act *as if* they remember things.  When *we*
remember, we're retrieving information that's stored *physically* in
the brain.  The zombie brain has this information too, so the zombie
can utter things like "I remember sliding down that hill as a child"
when they're in a situation that would make a human remember sliding
down that hill as a child.

### How could this be???

Now the question is, are zombies even *possible?*  You might think
that they're not.  You might think that

- crying because you're sad

just *is*

- crying because you're built to cry in the sort of situation you're
  in.

You might think those are just two ways of describing the same thing,
and that there's nothing *more* to being sad than being in a certain
physical state.  The argument might go something like this:

1. We're sad when and only when we're in physical state *S*.

2. Therefore, to be sad just *is* to be in physical state *S*.

3. Since zombies are physically indistinguishable from us, and we are
   sometimes sad, zombies are sometimes in physical state *S*.

4. But since to be sad just *is* to be in physical state *S*, zombies
   are sometimes sad.

5. But sadness is also a mental state, so zombies can (and sometimes
   do) have mental states.

We defined 'zombie' as something that *doesn't* have mental states.
Since we've now arrived at the contradictory conclusion that zombies
*do* have mental states, our definition of 'zombie' must have been
*logically inconsistent*---it allowed us to derive the contradiction
that zombies both do and do not have mental states.  "Zombies exist"
is therefore *logically impossible*, just as impossible as "Round
squares exist."

### NOT SO FAST

This is not a valid argument.

The weakest part of the argument is when we try to infer from (1) to
(2).  Just because I'm sad in all and only those situations when I'm
also in physical state *S*, why does that mean being sad *just is*
being in physical state *S*?  Suppose you could hypnotize a man or
condition him to get angry when and only when he hears Garrison
Keillor on the radio.  Would this mean that the man getting angry
*just is* the man hearing Garrison Keillor on the radio?

It's true that in this special case, hearing Garrison Keillor is a
pretty important part of how this man gets angry, but *there's more to
it than that.*

Likewise, being in physical state *S* is a pretty important part of
how and why we're sad, but you might think that there's more to it
than that physical state.

(Or you might not.)
