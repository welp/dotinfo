---
title: "UCSB Campus Resources"
date: 2014-01-01
tag: evergreen
---

College can be very stressful, especially for new and transferring students.
Fortunately, UCSB has a number of resources to make your time here easier.

<!--more-->

### CAPS (Counseling & Psychological Services)

If you're dealing with depression, anxiety, or are just feeling
overwhelmed, CAPS is available to help.  They offer individual and
group counseling, as well as relaxation rooms and other resources to
make the quarter more bearable.

- <tel:805-893-4411>

- <http://counseling.sa.ucsb.edu>

### CARE (Campus Advocacy Resources & Education)

CARE provides resources related to sexual assault, stalking, and
intimate partner violence.  They offer free and confidential
counseling, medical and legal assistance, and help with referrals for
long-term support for those who need it.

- <tel:805-893-4613>

- <http://wgse.sa.ucsb.edu/Care>

### RCSGD (Resource Center for Sexual and Gender Diversity)

The RCSGD provides resources for gender, sexual, romantic
minorities:

- lesbian
- gay
- bisexual
- transgender
- asexual
- intersex
- otherwise gender/sexual/romantic nonconforming individuals

They also collect reports of hate incidents and maintain a list of
gender neutral bathrooms on campus.

- <tel:805-893-5847>

- <http://wgse.sa.ucsb.edu/sgd/>

### CLAS (Campus Learning Assistance Services)

CLAS offers workshops on study skills like note-taking and time
management; group tutorials on math, science, and economics;
assistance with essay-writing, and other free services that will help
you perform will in your classes.

- <http://clas.sa.ucsb.edu>

### DSP (Disabled Students Program)

If you have any sort of disability---physical or mental, permanent or
temporary---we strongly encourage you to register with DSP.  They can
then notify your instructors if you need certain accomodations during
classes or during tests.

A disability can be something like difficulty seeing or hearing;
difficulty attending class due to physical or emotional distress;
chronic anxiety or depression; or anything else that interferes with
your schoolwork.

There's no shame in seeking help for these sorts of things, but
unfortunately UCSB requires that instructors receive confirmation from
DSP before providing accomodation.  Since DSP is often busy, contact
them as soon as possible if you need or expect to need their support.

- <tel:805-893-2668>

- <http://dsp.sa.ucsb.edu>
