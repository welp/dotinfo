---
title: "What’s the Dreaming Hypothesis, and why can’t we rule it out?"
author: "Alex Dunn"
date: 2014-03-03
course: phil1
quarter: w14
---

The second premise of the **Dream Argument** is that “I am not in a
position to eliminate (rule out) the Dreaming Hypothesis.”

The **Dreaming Hypothesis** is just this: “perhaps you are now
dreaming.”

Why should we accept the second premise of the Dream Argument? What
reasons are there to think that we’re *not* in a position to rule out
the Dreaming Hypothesis?

<!--more-->

We should first try to see if we actually *are* in a position to rule
out the Dreaming Hypothesis. If we can prove that we’re *not* in a
dream, then we *shouldn’t* accept the second premise of the Dream
Argument.

### How would we prove we’re not in a dream?

To prove we’re not in a dream right now, we need to know that there’s
something about our current experiences that couldn’t happen in a dream.
If I know it’s *impossible* that what’s happening right now could happen
in a dream, then I know I’m not in a dream. *What’s happening right now
has to pass the waking test*: if I’m dreaming, then it’s (logically)
impossible for my current experience to pass the test.

In other words,

-   an experience passes the waking test just in case it is (logically)
    impossible to be having that experience while dreaming.

### What would pass the waking test?

A waking test is one that we will necessarily *fail* if we are dreaming.

Let’s consider a possible test:

-   Scientists discoverd that it’s impossible to see fine details while
    dreaming. Does this mean that if I look at my hands, and can make
    out the fine lines on my palms, I know I’m not dreaming?

Not quite. It might be physically impossible to see the fine lines on
your palms while dreaming, but it’s not *logically* impossible. (Do you
remember why?)

Why does an experience pass the waking test only if it’s logically
impossible to have that experience while dreaming?

-   Suppose a scientist *dreams* that he discovers that it’s impossible
    to see fine details while dreaming.

Maybe that’s going on right now! Can we rule *that* out? I don’t think
we can. You’ve probably dreamed about learning something—where you left
your keys, maybe—that turned out to be false when you woke up. So maybe
that’s what’s going on right now: this is all one big dream, and the
scientists are part of the dream, and it’s only part of the dream that
we can’t see fine details while dreaming.

But things are different if it’s logically impossible to have a certain
experience while dreaming. Why?

-   Suppose a great mathematician proves, *in a dream*, that 2 + 2 = 4.

Now imagine he wakes up and writes down the proof in his dream journal.
If he did each step of the proof correctly in his dream, the proof will
be just as good when he wakes up. It won’t just be part of the dream.

**The rules of logic are the same whether we’re dreaming or not.** So if
it’s *logically impossible* to be having a certain experience while
dreaming, then it’s logically impossible whether we’re dreaming or not.
We don’t have to worry about scenarios like the dreaming scientist,
because logical truths that we discover while dreaming are true in the
waking world.

*****

Now the challenge is to discover one of these experiences that it’s
logically impossible to have while dreaming. We haven’t been able to
think of any so far. Can you?
