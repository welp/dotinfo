---
title: "Skepticism and common sense"
author: "Alex Dunn"
date: 2014-04-30
course: phil1
quarter: s14
---

Last week we talked about how one person's *modus ponens* is another's
*modus tollens*.  If you're given any argument of the form

1. If *P,* then *Q*
2. *P*
3. *Q*

there is another argument of the form

1. If *P,* then *Q*
2. Not *Q*
3. Not *P*

Both arguments are valid.  Which of the two arguments is *sound* (if
either are) depends on what *P* and *Q* are.

<!--more-->

*****

This week we've got another example of how an argument can be
reversed.  The simplest way to express the skeptical argument about
the external world is like this:

1. If I know I have hands, then I know there's an external world.
2. I do not know that there's an external world.
3. I do not know that I have hands.

The skeptic uses *modus tollens* to conclude that you actually don't
know even basic things like that you have hands!

But the skeptic's *modus tollens* is G.E. Moore's *modus ponens*.  He
responds with this argument:

1. If I know I have hands, then I know there's an external world.
2. I know I have hands.
3. I know there's an external world.

*****

Remember, both the skeptic and Moore have valid arguments.  They both
reason correctly from their premises.  But who has better premises?
