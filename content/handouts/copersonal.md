---
title: "What is Co-Personality?"
author: "Alex Dunn"
date: 2015-05-13
course: phil1
quarter: s15
---

> Think of your life as a long story. Let the story be a rather
> narcissistic story: cut out all details about everything else except
> you. So the story begins with an infant (or perhaps a fetus). It
> describes the infant developing into a child and then an
> adolescent. The adolescent passes into young adulthood, then
> adulthood, middle age, and finally old age and death. Like all
> stories, this story has parts. We can distinguish the part of the
> story concerning childhood from the part concerning adulthood. Given
> enough details, there will be parts concerning individual days,
> minutes, or even instants.

<!--more-->

> [There's a sense in which] persons are a lot like their
> stories. Just as my story has a part for my childhood, so I have a
> part consisting just of my childhood. Just as my story has a part
> describing just this instant, so I have a part that is
> me-at-this-very-instant (from page 1 of Ted Sider's
> *Four-Dimensionalism*).

This is a pretty good explanation of what *person-stages* are. Sider
uses "parts" instead of "person-stages", but he's talking about the
same thing. "Parts" might seem a lot more natural, but when we talk
about the parts of a person we're usually talking about their
*spatial* parts---my legs, my arms, my head. But right now we're
interested in *temporal* parts---my childhood, my teenage years, my
twenties.  "Person-stages" makes it clear that we're not talking about
legs and arms and heads.

So the idea is that a person is a collection of person-stages.  We are
the sum of our (temporal) parts.

Now, take any two random person-stages.  Either they're part of the
same "collection", or they're not.  Either they're part of the same
person or their not.  Either they're *co-personal* or they're not.
(Those are three ways of saying the same thing.)

Take my childhood and my twenties.  They're part of the same
person---me.  But *why?* What *makes it true* that a child in the
1990s and a 20-something now are parts of the same story---parts of
the same person?  Why are they *co-personal?*

### Bodily Continuity

<dl>
<dt>Person-stages A and B are co-personal</dt>
<dd>A and B have the same (i.e., numerically identical) body
</dd>
</dl>

My childhood and my twenties are part of the same person because they
have the same body.  It's not *qualitatively* identical—I've grown,
I've had my wisdom teeth removed, I've lost some blood—but it's
*numerically* the same body.

(How do we know it's numerically the same?)

### Soul Theory

<dl>
<dt>Person-stages A and B are co-personal</dt>
<dd>A and B have the same immaterial mental substance—the same soul
</dd>
</dl>

My childhood and my twenties are part of the same person because they
share the same soul.

### Memory Theory

<dl>
<dt>Person-stages A and B are co-personal</dt>
<dd>A and B are appropriately linked by memory
</dd>
</dl>

My childhood and my twenties are part of the same person because I, in
my twenties, have *true* memories of my childhood.

(In my twenties I can't remember being 1 year old.  Was that a
different person?)

### Psychological Continuity Theory

<dl>
<dt>Person-stages A and B are co-personal</dt>
<dd>A and B are psychologically continuous
</dd>
</dl>

My childhood and my twenties are part of the same person because the
psychologies of the person-stages are connected in the right way.
They're not connected directly—my teenage years are in between—but
my twenties are connected to my teenage years which are connected to
my childhood, and that's enough.

(What sort of connection does there need to be?  If the psychology of
my twenties is very similar to the psychology of my teenage years,
which in turn is very similar to that of my childhood, is that
sufficient?  Or is there something more than similarity connecting
these person-stages?)
