---
title: "Is psychological continuity what matters for personal identity?"
author: "Alex Dunn (TA for Philosophy 1)"
date: 2014-03-06
course: phil1
quarter: w14
---

If you accept the *Revised* Lockean Theory of personal identity, this
is what you mean by "psychological continuity":

Psychological continuity
:   Person-stages *A* and *B* are psychologically continuous just in
    case (1) *A* and *B* have very similar mental "profiles", and (2)
    that similarity is due to a reliable causal connection.
	
<!--more-->

(Notice that psychological continuity in this sense is not *transitive*.)

You can then say that what matters for personal identity is this sort
of continuity:

> Person-stages *A* and *B* are co-personal if and only if (1) *A* and
> *B* are psychologically continuous **or** (2) *A* and *B* are
> connected by a chain of psychologically continuous person-stages.

Because the sort of psychological continuity we're using isn't
transitive, clause (1) by itself isn't good enough.  The little boy
and the old general are not psychologically continuous, so we need
clause (2) to explain why they are nonetheless co-personal.  The old
general is connected to the little boy by a chain of psychologically
continuous person-stages (including the brave officer).

### Teleportation and Duplication

But now teleportation and duplication are supposed to cause problems
for this Revised Lockean Theory.

Let's suppose technology has advanced to the point where a machine can
scan you and then build a perfect replica of you.  One way this
technology might be put into use would be to provide extremely fast
transportation.  You could be scanned in Santa Barbara and replicated
in Los Angeles in the space of a few minutes.  Your old body would be
discarded, and there you are, in Los Angeles.

Why is this sort of **teleportation** supposed to cause a problem for
our theory?  The intuition, which you may or may not share, is that
it's not *you* in Los Angeles.  The intuition is that you were
*killed* after being scanned, and a person was *made* in Los Angeles
who happens to look and act just like you.  But our theory seems to
say that, no, that's really *you* in Los Angeles.

****

The problems caused by **duplication** are more obvious.  Suppose you
are replicated in Los Angeles, but your "old" body is *not*
discarded.  Are there now two versions of you?  Is the Santa Barbara
version of you the "real" you, because it's older?

But what if you're replicated in Los Angeles and San Diego and your
old body *is* discarded.  Neither of the two versions of you are older
than the other; which one is you?

****

The Revised Lockean Theory could be revised *again* to deal with
duplication:

> Person-stages *A* and *B* are co-personal if and only if (1) *A* and
> *B* are **uniquely** psychologically continuous **or** (2) *A* and
> *B* are connected by a chain of **uniquely** psychologically
> continuous person-stages.

Is this a satisfactory revision?
