---
title: "The Prince and the Cobbler"
author: "Alex Dunn"
date: 2015-05-06
course: phil1
quarter: s15
---

> The cobbler is a revolutionary plotting with his sister to
> assassinate the king.  They have decided to attempt the
> assassination on Christmas Day.  On Christmas Eve, the cobbler goes
> to sleep in his small house, and the prince goes to sleep in his
> palace.  Each are watched over as they sleep: the cobbler's sister
> was kept up by a tooth-ache and the royal family is guarded at all
> times.  None of the watchers notice anything out of the ordinary.
>
> But the man who wakes up in the cobbler's bed and sees the cobbler's
> sister smiling down at him has no recollection of her.  Likewise,
> the man who wakes up in the prince's bed, a butler standing at his
> bedside, cannot remember ever being inside a palace before.

<!--more-->

> The man in the cobbler's bed runs out of the cobbler's house, all
> the way to the gates of the prince's palace.  The guards do not
> recognize the man before them, but he knows the secret password and
> so they let him in.
>
> The man in the prince's bed is terrified to be surrounded by palace
> guards, but also believes that this may be a perfect opportunity to
> commit the assassination he seems to remember planning with his
> sister.

In short, the body of the prince appears to have all the mental states
of the cobbler, and the body of the cobbler appears to have all the
mental states of the prince.  The "prince" *seems* to remember
plotting to kill the king.  The "cobbler" *seems* to remember the
password to enter the palace.

We can illustrate the situation like this:

<figure>
<img src="/img/person-slices.sm.jpg" alt="The thin horizontal lines on top represent the non-mental portions of the princely person-stages. The thick horizontal lines on the bottom represent the non-mental portions of the cobbled person-stages. The diagonal lines represent the mental portions of the princely person-stages. The cross-hatching represents the mental portions of the cobbled person-stages." /><figcaption><em>The thin horizontal lines on top represent the non-mental portions of the princely person-stages. The thick horizontal lines on the bottom represent the non-mental portions of the cobbled person-stages. The diagonal lines represent the mental portions of the princely person-stages. The cross-hatching represents the mental portions of the cobbled person-stages.</em></figcaption>
</figure>

Now, the question is, does personal identity follow non-mental bodily
continuity (the horizontal lines) through time, or mental continuity
(the diagonal and cross-hatched lines)?
