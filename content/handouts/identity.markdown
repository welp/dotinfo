---
title: "Persons and Person-stages"
author: "Alex Dunn"
date: 2014-02-17
course: phil1
quarter: w14
---

> Think of your life as a long story. Let the story be a rather
> narcissistic story: cut out all details about everything else except
> you. So the story begins with an infant (or perhaps a fetus). It
> describes the infant developing into a child and then an
> adolescent. The adolescent passes into young adulthood, then
> adulthood, middle age, and finally old age and death. Like all
> stories, this story has parts. We can distinguish the part of the
> story concerning childhood from the part concerning adulthood. Given
> enough details, there will be parts concerning individual days,
> minutes, or even instants.

<!--more-->

> [There's a sense in which] persons are a lot like their
> stories. Just as my story has a part for my childhood, so I have a
> part consisting just of my childhood. Just as my story has a part
> describing just this instant, so I have a part that is
> me-at-this-very-instant (from page 1 of Ted Sider's
> *Four-Dimensionalism*).

This is a pretty good explanation of what *person-stages* are. Sider
uses "parts" instead of "person-stages", but he's talking about the
same thing. "Parts" might seem a lot more natural, but when we talk
about the parts of a person we're usually talking about their *spatial*
parts---my legs, my arms, my head. But right now we're interested in
*temporal* parts---my childhood, my teenage years, my 20s.
"Person-stages" makes it clear that we're not talking about legs and
arms and heads.

So the idea is that a person is a collection of person-stages.  We are
the sum of our (temporal) parts.

Now, take any two random person-stages.  Either they're part of the
same "collection", or they're not.  Either they're part of the same
person or their not.  Either they're *co-personal* or they're not.
(Those are three ways of saying the same thing.)

Take my childhood and my 20s.  They're part of the same person---me.
But *why?* What *makes it true* that a child in the 1990s and a
20-something now are parts of the same story---parts of the same
*person?*

### The Prince and the Cobbler

Here's a more elaborate version of the story of the Prince and the
Cobbler:

> The cobbler is a revolutionary plotting with his sister to
> assassinate the king.  They have decided to attempt the
> assassination on Christmas Day.  On Christmas Eve, the cobbler goes
> to sleep in his small house, and the prince goes to sleep in his
> palace.  Each are watched over as they sleep: the cobbler's sister
> was kept up by a tooth-ache and the royal family is guarded at all
> times.  None of the watchers notice anything out of the ordinary.

> But the man who wakes up in the cobbler's bed and sees the cobbler's
> sister smiling down at him has no recollection of her.  Likewise,
> the man who wakes up in the prince's bed, a butler standing at his
> bedside, cannot remember ever being inside a palace before.

> The man in the cobbler's bed runs out of the cobbler's house, all
> the way to the gates of the prince's palace.  The guards do not
> recognize the man before them, but he knows the secret password and
> so they let him in.

> The man in the prince's bed is terrified to be surrounded by palace
> guards, but also believes that this may be a perfect opportunity to
> commit the assassination he seems to remember planning with his
> sister.

In short, the body of the prince appears to have all the mental states
of the cobbler, and the body of the cobbler appears to have all the
mental states of the prince.  The "prince" *seems* to remember
plotting to kill the king.  The "cobbler" *seems* to remember the
password to enter the palace.

We can illustrate the situation like this:

<figure>
<img src="/img/person-slices.sm.jpg" alt="The thin horizontal lines on top represent the non-mental portions of the princely person-stages. The thick horizontal lines on the bottom represent the non-mental portions of the cobbled person-stages. The diagonal lines represent the mental portions of the princely person-stages. The cross-hatching represents the mental portions of the cobbled person-stages." /><figcaption><em>The thin horizontal lines on top represent the non-mental portions of the princely person-stages. The thick horizontal lines on the bottom represent the non-mental portions of the cobbled person-stages. The diagonal lines represent the mental portions of the princely person-stages. The cross-hatching represents the mental portions of the cobbled person-stages.</em></figcaption>
</figure>

Now, the question is, does personal identity follow non-mental bodily
continuity (the horizontal lines) through time, or mental continuity
(the diagonal and cross-hatched lines)?
