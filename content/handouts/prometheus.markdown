---
title: "The Prometheus Problem"
author: "Alex Dunn"
date: 2014-04-09
course: phil1
quarter: s14
---

Here's Paley's argument, direct from Tim's slides:

1. Watches have complex features the best explanation of which is that
   the watch was created by an intelligent designer for some purpose.

2. Living organisms are similar to watches in these respects.

3. Therefore, the best explanation of the complexities that we find in
   living organisms is that living organisms were created by an
   intelligent designer for some purpose.

<!--more-->

Richard Dawkins makes some good points in his discussion of this
argument.  Again, from Tim's slides:

> The *best* kinds of explanations of complex phenomena explaining the
> complexity in terms of *simpler* phenomena.

Therefore,

> Design explanations of complex phenomena should be a theoretic last
> resort, at least in the absence of direct evidence of design
> (e.g. you watch someone make a watch).

With this in mind, when we look back at the first premise of Paley's
argument, we realize that one of the watch's "features" that makes a
designer part of the best explanation for the watch is the feature of
*having been observed to be designed.*

If we had never seen a watch being made---or had not been told by a
trustworthy person that watches are designed and made by humans, the
best explanation might *not* be an intelligent designer.  Because, *in
general,* the best explanations of complex things don't require
something even more complicated.  A human watchmaker is more
complicated than a watch, so if we didn't already *know* that
watchmakers exist the best explanation of a watch would not require a
watchmaker.

****

OK, but how does this relate to God?  Dawkins thinks that we are like
watches who do *not* know that there is a watchmaker.  We have not
ourselves seen God, and---according to Dawkins---we do not have the
same amount of evidence for God's existence as we do for a human
watchmaker's existence.  Even if you've never seen a human watchmaker,
you've got lots of evidence that they exist.  That's why, when you see
a watch, you're right to assume that it was made by a watchmaker.

Dawkins thinks we don't have as much evidence for God's existence, so
we're *not* right to assume that we were made by God.

*****

Here's an objection to Dawkins.  His argument makes two assumptions:

1. God is more complex than we are, just as the watchmaker is more
   complex than the watch.

2. The existence of God requires explanation.

But are either of these assumptions true?
