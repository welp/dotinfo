---
Title: How Do We Talk About God in a World of Religious Pluralism?
Author: Alex Dunn
Date: 2015-01-15
course: phil1
quarter: w15
---

We've already seen in lecture that people mean different things by
"God."  It's because we arrive with different understandings of
concepts like *God* that we're going to be using a lot of definitions
in this class, to make sure everybody means the same thing when we're
talking about God and evil and so on.

<!--more-->

This means that if we're going to talk about God, we'll need
to---temporarily---agree which god(s) we're going to talk about.  For
historical and socioligical reasons, most of the philosophers of
religion in Western departments are Christian or come from a culture
heavily influenced by Christianity; that's why the God we use in our
discussions is most similar to the Judeo-Christian God of the Old and
New Testaments.

We're *not* trying to insist that the Judeo-Christian God exists, or
even that it's more likely that this God exists rather than another
god or set of gods.  Unfortunately most of us just aren't familiar
enough with other religions to discuss them in detail.

*****

But even within the Judeo-Christian tradition there are many different
views about what God is like.  The God of the Old Testament seems to
be more jealous and vengeful than the God depicted in the New
Testament, and some believers emphasize the former or the latter
depending on their belief about what God *should* be like.

An extreme example on one end would be the Westboro Baptist Church.
But just because they are Christians and hold those views, doesn't
mean that any other Christians have to share them.  And of course,
most don't.  Some hold related but much less vicious views about
things like homosexuality citing various passages of the Bible, while
many Christians believe that there is nothing in their faith that
compels them to discriminate against people whose sexuality and gender
differ from the norm.

Lots of atheists assume that because they read a certain passage of
the Bible critical of homosexuality, or because their religious
relative are against abortion, that being critical of homosexuality
and being against abortion is part of what it *means* to be
Christian.  The philosopher Michael Ruse, who really ought to know
better, published an article recently called
[Atheism: above all a moral issue](http://blog.oup.com/2015/01/atheism-moral-issue/)
in which he said this:

> Abortion, gay marriage, civil rights---all of these thorny issues
> and more are moral and social issues at the heart of our lives and
> what you believe about God is going to influence how you decide.

But that's totally not true; there are lots of Christians, Jews,
Muslims, etc. who support access to abortion, and there are atheists
who don't.  The same goes for those others issues.  The reason why is
that being a Christian really doesn't require anything more than
belief in what Professor Hess called the Supernatural Worldview:

1. God exists and is morally perfect, all-powerful, and
all-knowing. God is spirit (not material). There are no other gods.

2. The World consists of God and his creation. Some of creation is
immaterial and some of it is material.

3. God is the explanation for why there is something rather than
   nothing. There is something because God has always existed and must
   exist (just as two plus two must equal four). The universe and
   everything else besides God were created by God at some point in
   the past.

4. Humans were created by God to love, serve, and enjoy him
   forever. So, humans have a purpose. Humans have the free will to
   love and serve God or not. Human history is nothing more than the
   working out of the consequences of the fact that some people have
   chosen to do what they were created to do and some have chosen to
   not do what they were created to do.

You could probably be a Christian and reject (4), maybe even (3).
Christians and atheists both like to add requirements to being a good
Christian---the Christians do it so that in order to be a good person
at all, you need to be Christian, while the athiests try to show that
you *can't* be a good person if you're a Christian.
