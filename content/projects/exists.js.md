{
    "title": "Exists.js",
    "date": "2014-02-28",
    "link": "/exists"
}

Exists.js is a silly tool to generate symbolic representations of existence statements, such as "at least five things exist", "exactly two things exist", and "at most nine things exist".

If you're a scumbag analytic philosopher like me then this might actually
come in handy, like when your so-called "friend" suggests that chairs are
ontologically innocent.
