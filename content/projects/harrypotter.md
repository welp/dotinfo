{
    "title": "<em>Harry Potter</em> and philosophy",
    "date": "2014-06-19",
    "link": "http://baruffio.com/t/philosophy/"
}

I wasn't invited to contribute to the "Ultimate" [*Harry Potter* and Philosophy](http://www.wiley.com/WileyCDA/WileyTitle/productCd-0470398256.html) book, but I've written a few introductions to philosophical ideas (time travel, philosophy of names) using *Harry Potter* as a jumping-off point. Comments are always appreciated!