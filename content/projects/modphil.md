---
title: "<em>Modern Philosophy</em> in plain text"
date: 2012-05-28
link: /modernphil
---

*Modern Philosophy* is a free textbook created by
[Walter Ott](https://filebox.vt.edu/users/ottw/ott.htm "Walter Ott's
personal site"). It combines public-domain primary sources with
supplementary material and study questions. Everything not in the
public domain is licensed under
[CC BY-NC-SA 3.0](http://creativecommons.org/licenses/by-nc-sa/3.0/
"Creative Commons license page").

<!--more-->

This modified version of the text is written in
[Markdown](http://daringfireball.net/projects/markdown/ "Markdown
project page"). Markdown is a plain-text syntax that allows for easy
conversion to other file formats.  Using
[pandoc](http://johnmacfarlane.net/pandoc/ "pandoc project page"),
*Modern Philosophy* can be easily converted to HTML, PDF, EPUB, and
[many other formats](http://johnmacfarlane.net/pandoc/README.html#options
"pandoc options").
