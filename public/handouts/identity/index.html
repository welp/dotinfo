<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Persons and Person-stages — alexdunn.info</title>

<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="description" content="Alex Dunn is a PhD student at UCSB" />
<meta name="author" content="Alex Dunn" />
<meta name="keywords" content="ucsb, philosophy" />

  	<link href="/stylesheets/screen.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="/stylesheets/print.css" rel="stylesheet" type="text/css" media="print" />
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
		
	

</head>
<body>


<p class="assistive-text"><a href="#content-start">Skip to main content</a></p>

<header class="page-header" role="banner">
<h1 class="headline"><a href="/" title="main page">Alex Dunn</a></h1>
<ul class="top">
<li class="top"><script type="text/javascript">
<!--
h='&#x75;&#x6d;&#x61;&#x69;&#108;&#46;&#x75;&#x63;&#x73;&#98;&#46;&#x65;&#100;&#x75;';a='&#64;';n='&#x61;&#100;&#x75;&#110;&#110;';e=n+a+h;
document.write('<a h'+'ref'+'="ma'+'ilto'+':'+e+'">'+e+'<\/'+'a'+'>');

</script><noscript>&#x61;&#100;&#x75;&#110;&#110;&#32;&#x61;&#116;&#32;&#x75;&#x6d;&#x61;&#x69;&#108;&#32;&#100;&#x6f;&#116;&#32;&#x75;&#x63;&#x73;&#98;&#32;&#100;&#x6f;&#116;&#32;&#x65;&#100;&#x75;</noscript>
</li>
<li class="top after"><a href="http://pastebin.com/raw.php?i=CgtqpXYt" title="Pastebin">Public key</a></li>
<li class="top after"><a href="/cv">CV</a></li>
</ul>
</header>


<main id="content-start" role="main">

<section class="single handouts">
<h2>Persons and Person-stages
	
</h2>



<blockquote>
<p>Think of your life as a long story. Let the story be a rather
narcissistic story: cut out all details about everything else except
you. So the story begins with an infant (or perhaps a fetus). It
describes the infant developing into a child and then an
adolescent. The adolescent passes into young adulthood, then
adulthood, middle age, and finally old age and death. Like all
stories, this story has parts. We can distinguish the part of the
story concerning childhood from the part concerning adulthood. Given
enough details, there will be parts concerning individual days,
minutes, or even instants.</p>

<p>[There&rsquo;s a sense in which] persons are a lot like their
stories. Just as my story has a part for my childhood, so I have a
part consisting just of my childhood. Just as my story has a part
describing just this instant, so I have a part that is
me-at-this-very-instant (from page 1 of Ted Sider&rsquo;s
<em>Four-Dimensionalism</em>).</p>
</blockquote>

<p>This is a pretty good explanation of what <em>person-stages</em> are. Sider
uses &ldquo;parts&rdquo; instead of &ldquo;person-stages&rdquo;, but he&rsquo;s talking about the
same thing. &ldquo;Parts&rdquo; might seem a lot more natural, but when we talk
about the parts of a person we&rsquo;re usually talking about their <em>spatial</em>
parts&mdash;my legs, my arms, my head. But right now we&rsquo;re interested in
<em>temporal</em> parts&mdash;my childhood, my teenage years, my 20s.
&ldquo;Person-stages&rdquo; makes it clear that we&rsquo;re not talking about legs and
arms and heads.</p>

<p>So the idea is that a person is a collection of person-stages.  We are
the sum of our (temporal) parts.</p>

<p>Now, take any two random person-stages.  Either they&rsquo;re part of the
same &ldquo;collection&rdquo;, or they&rsquo;re not.  Either they&rsquo;re part of the same
person or their not.  Either they&rsquo;re <em>co-personal</em> or they&rsquo;re not.
(Those are three ways of saying the same thing.)</p>

<p>Take my childhood and my 20s.  They&rsquo;re part of the same person&mdash;me.
But <em>why?</em> What <em>makes it true</em> that a child in the 1990s and a
20-something now are parts of the same story&mdash;parts of the same
<em>person?</em></p>

<h3 id="the-prince-and-the-cobbler:4482dd09d228991e33ee8cd81fd9e19c">The Prince and the Cobbler</h3>

<p>Here&rsquo;s a more elaborate version of the story of the Prince and the
Cobbler:</p>

<blockquote>
<p>The cobbler is a revolutionary plotting with his sister to
assassinate the king.  They have decided to attempt the
assassination on Christmas Day.  On Christmas Eve, the cobbler goes
to sleep in his small house, and the prince goes to sleep in his
palace.  Each are watched over as they sleep: the cobbler&rsquo;s sister
was kept up by a tooth-ache and the royal family is guarded at all
times.  None of the watchers notice anything out of the ordinary.</p>

<p>But the man who wakes up in the cobbler&rsquo;s bed and sees the cobbler&rsquo;s
sister smiling down at him has no recollection of her.  Likewise,
the man who wakes up in the prince&rsquo;s bed, a butler standing at his
bedside, cannot remember ever being inside a palace before.</p>

<p>The man in the cobbler&rsquo;s bed runs out of the cobbler&rsquo;s house, all
the way to the gates of the prince&rsquo;s palace.  The guards do not
recognize the man before them, but he knows the secret password and
so they let him in.</p>

<p>The man in the prince&rsquo;s bed is terrified to be surrounded by palace
guards, but also believes that this may be a perfect opportunity to
commit the assassination he seems to remember planning with his
sister.</p>
</blockquote>

<p>In short, the body of the prince appears to have all the mental states
of the cobbler, and the body of the cobbler appears to have all the
mental states of the prince.  The &ldquo;prince&rdquo; <em>seems</em> to remember
plotting to kill the king.  The &ldquo;cobbler&rdquo; <em>seems</em> to remember the
password to enter the palace.</p>

<p>We can illustrate the situation like this:</p>

<figure>
<img src="/img/person-slices.sm.jpg" alt="The thin horizontal lines on top represent the non-mental portions of the princely person-stages. The thick horizontal lines on the bottom represent the non-mental portions of the cobbled person-stages. The diagonal lines represent the mental portions of the princely person-stages. The cross-hatching represents the mental portions of the cobbled person-stages." /><figcaption><em>The thin horizontal lines on top represent the non-mental portions of the princely person-stages. The thick horizontal lines on the bottom represent the non-mental portions of the cobbled person-stages. The diagonal lines represent the mental portions of the princely person-stages. The cross-hatching represents the mental portions of the cobbled person-stages.</em></figcaption>
</figure>

<p>Now, the question is, does personal identity follow non-mental bodily
continuity (the horizontal lines) through time, or mental continuity
(the diagonal and cross-hatched lines)?</p>

</section>

<footer class="single handouts">
	<p>Posted on <time datetime="2014-02-17 00:00:00 &#43;0000 UTC">17 February 2014</time>. See <a href="/handouts">all handouts</a>.</p>
</footer>

</main>
<footer class="page-footer" role="contentinfo">
<p>Content copyright © 2014 <a href="/" title="main page">Alex Dunn</a>. Email me at <script type="text/javascript">
<!--
h='&#x75;&#x6d;&#x61;&#x69;&#108;&#46;&#x75;&#x63;&#x73;&#98;&#46;&#x65;&#100;&#x75;';a='&#64;';n='&#x61;&#100;&#x75;&#110;&#110;';e=n+a+h;
document.write('<a h'+'ref'+'="ma'+'ilto'+':'+e+'">'+e+'<\/'+'a'+'>');

</script><noscript>&#x61;&#100;&#x75;&#110;&#110;&#32;&#x61;&#116;&#32;&#x75;&#x6d;&#x61;&#x69;&#108;&#32;&#100;&#x6f;&#116;&#32;&#x75;&#x63;&#x73;&#98;&#32;&#100;&#x6f;&#116;&#32;&#x65;&#100;&#x75;</noscript> (<a href="http://pastebin.com/raw.php?i=CgtqpXYt" title="Pastebin">public key</a>).</p>
</footer>

</body>
</html>
