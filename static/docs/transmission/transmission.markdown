# The Transmissibility Argument

## 8 May 2014, for [Phil 1](../../s14.html)

Most of the arguments we've looked at so far have two premises and a
conclusion.  The **Transmissibility Argument** has *three* premises,
and the third is pretty complex, so this argument is a bit more
difficult to understand than the other ones we've looked at.  Here it
is:

1. *S* cannot know that *SK* is false.

2. *O* implies that *SK* is false, and *S* knows this.

3. If *S* knows that *O* is true, and that *O* implies that *SK* is
   false, then *S* can know that *SK* is false.

4. Therefore, *S* does not know *O.*

Let's replace *S* and *SK* and *O* with specific examples to make this
less abstract:

1. I cannot know that the brain-in-a-vat scenario is false.

2. *I have hands* implies that the brain-in-a-vat scenario is false,
   and I know this.

3. If I know that *I have hands* is true, and that *I have hands*
   implies that the brain-in-a-vat scenario is false, then I can know
   that the brain-in-a-vat scenario is false.

4. Therefore, I don't know *I have hands.*

Premise 2 could also be written like this:

> I know this: *I have hands* implies that the brain-in-a-vat scenario
> is false.

What premise 2 says is just that I recognize that my having hands is
incompatible with the brain-in-a-vat scenario.  If I'm a brain in a
vat, I don't have hands---I'm nothing but a brain!  But that means
that if I *do* have hands, I'm not a brain in a vat.  Premise 2 is
claiming that I know all this---I know that *I have hands* implies
that the brain-in-a-vat scenario is false.

Premise 3 looks complicated, but it's really just one big conditional
statement.  It's just a really messy <span class="connector">If</span>
<span class="antecedent">P</span>, <span class="connector">then</span>
<span class="consequent">Q</span>:

> <span class="connector">If</span> <span class="antecedent">I know
> that *I have hands* is true, and that *I have hands* implies that
> the brain-in-a-vat scenario is false</span>, <span
> class="connector">then</span> <span class="consequent">I can know
> that the brain-in-a-vat scenario is false.</span>

Since it's basically *if P, then Q,* the *modus ponens* and *modus
tollens* rules apply to premise 3.  If *P*---the antecedent---is true,
then (by *modus ponens*), the consequent---*Q*---must be true as well.
Or if the consequent is false, then (by *modus tollens*) antecedent
must be false as well.

Now look at premise 1.  *I cannot know that the brain-in-a-vat
scenario is false.* That's just the negation of the consequent of
premise 3!  So, using *modus tollens,* we can infer that the
antecedent is false.  In other words, it's *not* true that

> I know that *I have hands* is true, and that *I have hands* implies
> that the brain-in-a-vat scenario is false.

The antecedent of premise 3 is a conjunction.  It makes two claims:

1. I know that *I have hands* is true.

2. I know that *I have hands* implies that the brain-in-a-vat scenario
   is false.

If a conjunction is false, that means that *at least* one of its
conjuncts is false.  Suppose that I tell you that at 9AM, I'll be in
my office *and* at Nicoletti's.  What I told you was false.  I won't
be in both places at once (that's impossible), but just because that
conjunction is false doesn't mean that I won't be in my office *or* at
Nicoletti's.  When a conjunction is false, that doesn't mean that
*both* conjuncts are false; it just means that *at least* one is.

So let's get back to premise 3.  The antecedent of premise 3 is a
conjunction, and it's false.  So that means that at least one of its
conjuncts is false.  But which one?  How can we decide between them?

Fortunately (or unfortunately), the decision has already been made for
us.  Look at premise 2:

2. *I have hands* implies that the brain-in-a-vat scenario is false,
   and I know this.

Remember that this can also be written as:

> I know that *I have hands* implies that the brain-in-a-vat scenario
> is false.

That should look familiar, because it's the second conjunct of the
antecedent of premise 3.  So the second conjunct of the antecedent of
premise 3 is true.  This is perfectly compatible with the whole
conjunction being false---it might be true that I'm in my office, even
though it's false that I'm in my office *and* at Nicoletti's.  When a
conjunction is false that just means that *at least* one conjunct is
false.  They can't both be true.

But if the second conjunct of the antecedent of premise 3 is true,
then the first conjunct has to be *false.*  Since the whole
conjunction is false, at least one of the conjuncts has to be false.
They can't both be true.  Therefore, the following is false:

1. I know that *I have hands* is true.

In other words, I don't know that I have hands.  Which is what the
skeptic has been trying to prove.  Using the Transmissibility Argument
takes a lot longer than using the Certainty Argument or the
Possibility-of-Error Argument, but it's a much more plausible
argument.
